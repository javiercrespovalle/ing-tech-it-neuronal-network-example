# ING Tech IT - Neuronal Network Example

## What is this repository for?

This is a demo about Neuronal Networks that is explained in an article published at ING Tech IT page (https://www.ingtechit.es/).The goal is show the main steps that are necessary 
when you create a AI system. It has been developed using Javascript and a library called "Neataptic" (https://wagenaartje.github.io/neataptic/). 

## How do I get set up?

* Install Node.JS
* Get dependencies: npm install neataptic
* Just type in your console: nodejs demo.js

